var express = require('express');
var app = express();
var now = new Date();
        apis = require('./apis/apis');

app.configure(function () {
    app.use(express.logger('dev'));
    app.use(express.bodyParser());
});

app.get('/', apis.getMain);
app.get('/apis', apis.getMain);

app.listen(7100);
console.log(now + ': Server listening on 7100');
